from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"{self._name} is eating {food}")

    def make_sound(self):
        print("Meow Meow")

    def call(self):
        print(f"{self._name} , come on!!")

class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"{self._name} is eating {food}")

    def make_sound(self):
        print("Arff Arff")

    def call(self):
        print(f"{self._name} , come on!!")




cat = Cat("Blue", "Persian", 2)
cat.eat("Sweet and Sour Fist")
cat.make_sound()
cat.call()



dog = Dog("Scoby", "Shihtzu", 3)
dog.eat("Humba")
dog.make_sound()
dog.call()
