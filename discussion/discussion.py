# Define the parent class
class Person:
    def __init__(self, name):
        self.name = name
        
    def get_name(self):
        return self.name

# Define the child class
class Student(Person):
    def __init__(self, name, student_no, course, year_level):
        super().__init__(name)
        self.student_no = student_no
        self.course = course
        self.year_level = year_level
        
    def get_student_no(self):
        return self.student_no
    
    def set_student_no(self, student_no):
        self.student_no = student_no
        
    def get_course(self):
        return self.course
    
    def set_course(self, course):
        self.course = course
        
    def get_year_level(self):
        return self.year_level
    
    def set_year_level(self, year_level):
        self.year_level = year_level
        
    def get_detail(self):
        print(f"{self.get_name()} is currently in year {self.year_level} taking up {self.course}")
    
# Create an instance of the Student class
student = Student("Zyver Jade Ponce", "202000001", "BSIS", 4)

# Call the get_detail method to print out student details
student.get_detail()
